const Dao = require("./dao.js");

module.exports = class PersonDao extends Dao {
  getAll(callback) {
    super.query("select navn, alder, adresse from person", [], callback);
  }

  getOne(id, callback) {
    super.query(
      "select navn, alder, adresse from person where id=?",
      [id],
      callback
    );
  }

  createOne(json, callback) {
    var val = [json.navn, json.adresse, json.alder];
    super.query(
      "insert into person (navn,adresse,alder) values (?,?,?)",
      val,
      callback
    );
  }

  deleteOne(id, callback) {
      var val = [id];
      super.query(
          "DELETE FROM person WHERE id = ?",
          val,
          callback
      );
  }

  updateOne(id, json, callback) {
      var val = [json.navn, json.adresse, json.alder, id];
      super.query(
          "UPDATE person SET navn = ?, adresse = ?, alder = ? WHERE id = ?",
          val,
          callback
      );
  }
};
